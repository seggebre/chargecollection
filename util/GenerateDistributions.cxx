#include "ChargeCollection/InputOptions.h"
#include "ChargeCollection/HistoProcessor.h"

#include <iostream>

int main(int argc, char *argv[]){

    using namespace ttHML_InputOptions;
    auto RunOptions = get_config_opts(argc, argv);

    for(unsigned int iInput =0; iInput < RunOptions.InputFiles.size(); iInput++){
        auto SampleList = SplitInput(RunOptions.InputFiles.at(iInput), ',');

        std::string OutputFileName = RunOptions.OutputDirectory + "Histograms_RunNumber_" + RunOptions.InputRunNumbers.at(iInput) + ".root";
        HistoProcessor* SampleProcess = new HistoProcessor(SampleList, OutputFileName, RunOptions.InputRunNumbers.at(iInput));
        SampleProcess->Process();
    }

    return EXIT_SUCCESS;
}