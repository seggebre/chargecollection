//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Oct 10 11:27:44 2022 by ROOT version 6.26/04
// from TTree nominal/tree
//////////////////////////////////////////////////////////

#ifndef Event_h
#define Event_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class Event {
public :
    TTree          *fChain;   //!pointer to the analyzed TTree or TChain
    Int_t           fCurrent; //!current Tree number in a TChain

    // Declaration of leaf types
    Int_t           runNumber;
    Int_t           eventNumber;
    Int_t           lumiBlock;
    Float_t         averagePU;
    Float_t         eventPU;
    Int_t           mcFlag;
    Float_t         pVtxX;
    Float_t         pVtxY;
    Float_t         pVtxZ;
    Float_t         pVtxXErr;
    Float_t         pVtxYErr;
    Float_t         pVtxZErr;
    Float_t         truthPVtxX;
    Float_t         truthPVtxY;
    Float_t         truthPVtxZ;
    Int_t           trackNPixelHits;
    Int_t           trackNPixelHoles;
    Int_t           trackNPixelLayers;
    Int_t           trackNPixelOutliers;
    Int_t           trackNIBLHits;
    Int_t           trackNSCTHits;
    Int_t           trackNSCTHoles;
    Int_t           trackNTRTHits;
    Int_t           trackNBLHits;
    Int_t           trackNSplitIBLHits;
    Int_t           trackNSplitBLHits;
    Int_t           trackNSharedIBLHits;
    Int_t           trackNSharedBLHits;
    Int_t           trackNL2Hits;
    Int_t           trackNL3Hits;
    Int_t           trackNECHits;
    Int_t           trackNIBLExpectedHits;
    Int_t           trackNBLExpectedHits;
    Int_t           trackNSCTSharedHits;
    Float_t         trackCharge;
    Float_t         trackPt;
    Float_t         trackPhi;
    Float_t         trackEta;
    Float_t         trackTheta;
    Float_t         trackqOverP;
    Float_t         trackD0;
    Float_t         trackZ0;
    Float_t         trackD0Err;
    Float_t         trackZ0Err;
    Float_t         trackqOverPErr;
    Float_t         trackDeltaZSinTheta;
    Float_t         trackPixeldEdx;
    Int_t           trackNPixeldEdx;
    Float_t         trueD0;
    Float_t         trueZ0;
    Float_t         truePhi;
    Float_t         trueTheta;
    Float_t         trueqOverP;
    Int_t           truePdgId;
    Int_t           trueBarcode;
    Float_t         truthMatchProb;
    Float_t         genVtxR;
    Float_t         genVtxZ;
    Int_t           parentFlavour;
    Float_t         minTrkdR;
    Int_t           hitSplitIBL;
    Int_t           hitSplitBL;
    Int_t           hitSplitL2;
    Int_t           hitSplitL3;
    std::vector<int>     *hitIsEndCap;
    std::vector<int>     *hitIsHole;
    std::vector<int>     *hitIsOutlier;
    std::vector<int>     *hitIs3D;
    std::vector<int>     *hitIsIBL;
    std::vector<int>     *hitLayer;
    std::vector<float>   *hitCharge;
    std::vector<int>     *hitToT;
    std::vector<int>     *hitLVL1A;
    std::vector<int>     *hitNPixelX;
    std::vector<int>     *hitNPixelY;
    std::vector<int>     *hitEtaModule;
    std::vector<int>     *hitPhiModule;
    std::vector<float>   *hitVBias;
    std::vector<float>   *hitVDep;
    std::vector<float>   *hitTemp;
    std::vector<float>   *hitLorentzShift;
    std::vector<int>     *hitIsSplit;
    std::vector<float>   *hitGlobalX;
    std::vector<float>   *hitGlobalY;
    std::vector<float>   *hitGlobalZ;
    std::vector<float>   *hitLocalX;
    std::vector<float>   *hitLocalY;
    std::vector<float>   *g4LocalX;
    std::vector<float>   *g4LocalY;
    std::vector<int>     *g4Barcode;
    std::vector<int>     *g4PdgId;
    std::vector<float>   *g4EnergyDeposit;
    std::vector<float>   *trkLocalX;
    std::vector<float>   *trkLocalY;
    std::vector<float>   *hitLocalErrorX;
    std::vector<float>   *hitLocalErrorY;
    std::vector<float>   *trkLocalErrorX;
    std::vector<float>   *trkLocalErrorY;
    std::vector<float>   *unbiasedResidualX;
    std::vector<float>   *unbiasedResidualY;
    std::vector<float>   *biasedResidualX;
    std::vector<float>   *biasedResidualY;
    std::vector<float>   *unbiasedPullX;
    std::vector<float>   *unbiasedPullY;
    std::vector<float>   *biasedPullX;
    std::vector<float>   *biasedPullY;
    std::vector<float>   *trk2G4DistX;
    std::vector<float>   *trk2G4DistY;
    std::vector<float>   *trk2DistX;
    std::vector<float>   *trk2DistY;
    std::vector<float>   *hit3DClusterDX;
    std::vector<float>   *hit3DClusterDY;
    std::vector<float>   *trkPhiOnSurface;
    std::vector<float>   *trkThetaOnSurface;
    std::vector<float>   *trkHitLocalX;
    std::vector<float>   *trkHitLocalY;
    std::vector<float>   *trkHitLocalErrorX;
    std::vector<float>   *trkHitLocalErrorY;
    std::vector<int>     *pixelClusterLayer;
    std::vector<int>     *pixelClusterIsSplit;
    std::vector<float>   *pixelClusterLocalX;
    std::vector<float>   *pixelClusterLocalY;
    std::vector<float>   *pixelClusterLocalErrorX;
    std::vector<float>   *pixelClusterLocalErrorY;
    std::vector<int>     *hitNContributingPtcs;
    std::vector<int>     *hitNContributingPU;
    std::vector<std::vector<int> > *hitContributingPtcsBarcode;
    std::vector<int>     *pixelIBLIndex;
    std::vector<int>     *pixelIBLRow;
    std::vector<int>     *pixelIBLCol;
    std::vector<int>     *pixelIBLCharge;
    std::vector<int>     *pixelIBLToT;
    std::vector<int>     *pixelBLIndex;
    std::vector<int>     *pixelBLRow;
    std::vector<int>     *pixelBLCol;
    std::vector<int>     *pixelBLCharge;
    std::vector<int>     *pixelBLToT;
    std::vector<int>     *pixelL2Index;
    std::vector<int>     *pixelL2Row;
    std::vector<int>     *pixelL2Col;
    std::vector<int>     *pixelL2Charge;
    std::vector<int>     *pixelL2ToT;
    std::vector<int>     *pixelL3Index;
    std::vector<int>     *pixelL3Row;
    std::vector<int>     *pixelL3Col;
    std::vector<int>     *pixelL3Charge;
    std::vector<int>     *pixelL3ToT;

    // List of branches
    TBranch        *b_runNumber;   //!
    TBranch        *b_eventNumber;   //!
    TBranch        *b_lumiBlock;   //!
    TBranch        *b_averagePU;   //!
    TBranch        *b_eventPU;   //!
    TBranch        *b_mcFlag;   //!
    TBranch        *b_pVtxX;   //!
    TBranch        *b_pVtxY;   //!
    TBranch        *b_pVtxZ;   //!
    TBranch        *b_pVtxXErr;   //!
    TBranch        *b_pVtxYErr;   //!
    TBranch        *b_pVtxZErr;   //!
    TBranch        *b_truthPVtxX;   //!
    TBranch        *b_truthPVtxY;   //!
    TBranch        *b_truthPVtxZ;   //!
    TBranch        *b_trackNPixelHits;   //!
    TBranch        *b_trackNPixelHoles;   //!
    TBranch        *b_trackNPixelLayers;   //!
    TBranch        *b_trackNPixelOutliers;   //!
    TBranch        *b_trackNIBLHits;   //!
    TBranch        *b_trackNSCTHits;   //!
    TBranch        *b_trackNSCTHoles;   //!
    TBranch        *b_trackNTRTHits;   //!
    TBranch        *b_trackNBLHits;   //!
    TBranch        *b_trackNSplitIBLHits;   //!
    TBranch        *b_trackNSplitBLHits;   //!
    TBranch        *b_trackNSharedIBLHits;   //!
    TBranch        *b_trackNSharedBLHits;   //!
    TBranch        *b_trackNL2Hits;   //!
    TBranch        *b_trackNL3Hits;   //!
    TBranch        *b_trackNECHits;   //!
    TBranch        *b_trackNIBLExpectedHits;   //!
    TBranch        *b_trackNBLExpectedHits;   //!
    TBranch        *b_trackNSCTSharedHits;   //!
    TBranch        *b_trackCharge;   //!
    TBranch        *b_trackPt;   //!
    TBranch        *b_trackPhi;   //!
    TBranch        *b_trackEta;   //!
    TBranch        *b_trackTheta;   //!
    TBranch        *b_trackqOverP;   //!
    TBranch        *b_trackD0;   //!
    TBranch        *b_trackZ0;   //!
    TBranch        *b_trackD0Err;   //!
    TBranch        *b_trackZ0Err;   //!
    TBranch        *b_trackqOverPErr;   //!
    TBranch        *b_trackDeltaZSinTheta;   //!
    TBranch        *b_trackPixeldEdx;   //!
    TBranch        *b_trackNPixeldEdx;   //!
    TBranch        *b_trueD0;   //!
    TBranch        *b_trueZ0;   //!
    TBranch        *b_truePhi;   //!
    TBranch        *b_trueTheta;   //!
    TBranch        *b_trueqOverP;   //!
    TBranch        *b_truePdgId;   //!
    TBranch        *b_trueBarcode;   //!
    TBranch        *b_truthMatchProb;   //!
    TBranch        *b_genVtxR;   //!
    TBranch        *b_genVtxZ;   //!
    TBranch        *b_parentFlavour;   //!
    TBranch        *b_minTrkdR;   //!
    TBranch        *b_hitSplitIBL;   //!
    TBranch        *b_hitSplitBL;   //!
    TBranch        *b_hitSplitL2;   //!
    TBranch        *b_hitSplitL3;   //!
    TBranch        *b_hitIsEndCap;   //!
    TBranch        *b_hitIsHole;   //!
    TBranch        *b_hitIsOutlier;   //!
    TBranch        *b_hitIs3D;   //!
    TBranch        *b_hitIsIBL;   //!
    TBranch        *b_hitLayer;   //!
    TBranch        *b_hitCharge;   //!
    TBranch        *b_hitToT;   //!
    TBranch        *b_hitLVL1A;   //!
    TBranch        *b_hitNPixelX;   //!
    TBranch        *b_hitNPixelY;   //!
    TBranch        *b_hitEtaModule;   //!
    TBranch        *b_hitPhiModule;   //!
    TBranch        *b_hitVBias;   //!
    TBranch        *b_hitVDep;   //!
    TBranch        *b_hitTemp;   //!
    TBranch        *b_hitLorentzShift;   //!
    TBranch        *b_hitIsSplit;   //!
    TBranch        *b_hitGlobalX;   //!
    TBranch        *b_hitGlobalY;   //!
    TBranch        *b_hitGlobalZ;   //!
    TBranch        *b_hitLocalX;   //!
    TBranch        *b_hitLocalY;   //!
    TBranch        *b_g4LocalX;   //!
    TBranch        *b_g4LocalY;   //!
    TBranch        *b_g4Barcode;   //!
    TBranch        *b_g4PdgId;   //!
    TBranch        *b_g4EnergyDeposit;   //!
    TBranch        *b_trkLocalX;   //!
    TBranch        *b_trkLocalY;   //!
    TBranch        *b_hitLocalErrorX;   //!
    TBranch        *b_hitLocalErrorY;   //!
    TBranch        *b_trkLocalErrorX;   //!
    TBranch        *b_trkLocalErrorY;   //!
    TBranch        *b_unbiasedResidualX;   //!
    TBranch        *b_unbiasedResidualY;   //!
    TBranch        *b_biasedResidualX;   //!
    TBranch        *b_biasedResidualY;   //!
    TBranch        *b_unbiasedPullX;   //!
    TBranch        *b_unbiasedPullY;   //!
    TBranch        *b_biasedPullX;   //!
    TBranch        *b_biasedPullY;   //!
    TBranch        *b_trk2G4DistX;   //!
    TBranch        *b_trk2G4DistY;   //!
    TBranch        *b_trk2DistX;   //!
    TBranch        *b_trk2DistY;   //!
    TBranch        *b_hit3DClusterDX;   //!
    TBranch        *b_hit3DClusterDY;   //!
    TBranch        *b_trkPhiOnSurface;   //!
    TBranch        *b_trkThetaOnSurface;   //!
    TBranch        *b_trkHitLocalX;   //!
    TBranch        *b_trkHitLocalY;   //!
    TBranch        *b_trkHitLocalErrorX;   //!
    TBranch        *b_trkHitLocalErrorY;   //!
    TBranch        *b_pixelClusterLayer;   //!
    TBranch        *b_pixelClusterIsSplit;   //!
    TBranch        *b_pixelClusterLocalX;   //!
    TBranch        *b_pixelClusterLocalY;   //!
    TBranch        *b_pixelClusterLocalErrorX;   //!
    TBranch        *b_pixelClusterLocalErrorY;   //!
    TBranch        *b_hitNContributingPtcs;   //!
    TBranch        *b_hitNContributingPU;   //!
    TBranch        *b_hitContributingPtcsBarcode;   //!
    TBranch        *b_pixelIBLIndex;   //!
    TBranch        *b_pixelIBLRow;   //!
    TBranch        *b_pixelIBLCol;   //!
    TBranch        *b_pixelIBLCharge;   //!
    TBranch        *b_pixelIBLToT;   //!
    TBranch        *b_pixelBLIndex;   //!
    TBranch        *b_pixelBLRow;   //!
    TBranch        *b_pixelBLCol;   //!
    TBranch        *b_pixelBLCharge;   //!
    TBranch        *b_pixelBLToT;   //!
    TBranch        *b_pixelL2Index;   //!
    TBranch        *b_pixelL2Row;   //!
    TBranch        *b_pixelL2Col;   //!
    TBranch        *b_pixelL2Charge;   //!
    TBranch        *b_pixelL2ToT;   //!
    TBranch        *b_pixelL3Index;   //!
    TBranch        *b_pixelL3Row;   //!
    TBranch        *b_pixelL3Col;   //!
    TBranch        *b_pixelL3Charge;   //!
    TBranch        *b_pixelL3ToT;   //!

    Event(TTree *tree);
    virtual ~Event();
    virtual Int_t    GetEntry(Long64_t entry);
    virtual Long64_t LoadTree(Long64_t entry);
    virtual void     Init(TTree *tree);
};

#endif
