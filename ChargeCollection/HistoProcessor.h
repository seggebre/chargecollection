#include "ChargeCollection/Event.h"

#include <iostream>
#include <string>
#include <TH1D.h>

class HistoProcessor{

    public:
        std::vector<std::string> m_InputFiles;
        std::string m_OutputFile;
        std::string m_RunNumber;

        HistoProcessor(std::vector<std::string> InputFiles, std::string OutputFile, std::string RunNumber);
        void Process();

    private:
        void RenameHistograms();
        void Fill_Histograms(Event event);
        void Write_Histograms();
        void FillHistogram_UnderOverflowBin(TH1D* Histogram, float Variable, float Weight, bool Overflow_Flag, bool Underflow_Flag);
        double GetSurfaceAngle(double Phi, double Theta);

        TH1D* m_Corrected_ClusterCharge_SideAC_Disk1 = new TH1D("Corrected_ClusterCharge_SideAC_Disk1", "Corrected_ClusterCharge_SideAC_Disk1", 120, 0, 120);
        TH1D* m_Corrected_ClusterCharge_SideAC_Disk2 = new TH1D("Corrected_ClusterCharge_SideAC_Disk2", "Corrected_ClusterCharge_SideAC_Disk2", 120, 0, 120);
        TH1D* m_Corrected_ClusterCharge_SideAC_Disk3 = new TH1D("Corrected_ClusterCharge_SideAC_Disk3", "Corrected_ClusterCharge_SideAC_Disk3", 120, 0, 120);

        TH1D* m_Corrected_ClusterCharge_SideA_Disk1 = new TH1D("Corrected_ClusterCharge_SideA_Disk1", "Corrected_ClusterCharge_SideA_Disk1", 120, 0, 120);
        TH1D* m_Corrected_ClusterCharge_SideA_Disk2 = new TH1D("Corrected_ClusterCharge_SideA_Disk2", "Corrected_ClusterCharge_SideA_Disk2", 120, 0, 120);
        TH1D* m_Corrected_ClusterCharge_SideA_Disk3 = new TH1D("Corrected_ClusterCharge_SideA_Disk3", "Corrected_ClusterCharge_SideA_Disk3", 120, 0, 120);

        TH1D* m_Corrected_ClusterCharge_SideC_Disk1 = new TH1D("Corrected_ClusterCharge_SideC_Disk1", "Corrected_ClusterCharge_SideC_Disk1", 120, 0, 120);
        TH1D* m_Corrected_ClusterCharge_SideC_Disk2 = new TH1D("Corrected_ClusterCharge_SideC_Disk2", "Corrected_ClusterCharge_SideC_Disk2", 120, 0, 120);
        TH1D* m_Corrected_ClusterCharge_SideC_Disk3 = new TH1D("Corrected_ClusterCharge_SideC_Disk3", "Corrected_ClusterCharge_SideC_Disk3", 120, 0, 120);
        
};
