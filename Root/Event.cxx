#include "ChargeCollection/Event.h"

Event::Event(TTree *tree){
    // if parameter tree is not specified (or zero), connect the file
    // used to generate this class and read the Tree.
    Init(tree);
}

Event::~Event(){
}

Int_t Event::GetEntry(Long64_t entry){

    //Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}

Long64_t Event::LoadTree(Long64_t entry){

    // Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
    }

    return centry;
}

void Event::Init(TTree *tree){

    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).

    // Set object pointer
    hitIsEndCap = 0;
    hitIsHole = 0;
    hitIsOutlier = 0;
    hitIs3D = 0;
    hitIsIBL = 0;
    hitLayer = 0;
    hitCharge = 0;
    hitToT = 0;
    hitLVL1A = 0;
    hitNPixelX = 0;
    hitNPixelY = 0;
    hitEtaModule = 0;
    hitPhiModule = 0;
    hitVBias = 0;
    hitVDep = 0;
    hitTemp = 0;
    hitLorentzShift = 0;
    hitIsSplit = 0;
    hitGlobalX = 0;
    hitGlobalY = 0;
    hitGlobalZ = 0;
    hitLocalX = 0;
    hitLocalY = 0;
    g4LocalX = 0;
    g4LocalY = 0;
    g4Barcode = 0;
    g4PdgId = 0;
    g4EnergyDeposit = 0;
    trkLocalX = 0;
    trkLocalY = 0;
    hitLocalErrorX = 0;
    hitLocalErrorY = 0;
    trkLocalErrorX = 0;
    trkLocalErrorY = 0;
    unbiasedResidualX = 0;
    unbiasedResidualY = 0;
    biasedResidualX = 0;
    biasedResidualY = 0;
    unbiasedPullX = 0;
    unbiasedPullY = 0;
    biasedPullX = 0;
    biasedPullY = 0;
    trk2G4DistX = 0;
    trk2G4DistY = 0;
    trk2DistX = 0;
    trk2DistY = 0;
    hit3DClusterDX = 0;
    hit3DClusterDY = 0;
    trkPhiOnSurface = 0;
    trkThetaOnSurface = 0;
    trkHitLocalX = 0;
    trkHitLocalY = 0;
    trkHitLocalErrorX = 0;
    trkHitLocalErrorY = 0;
    pixelClusterLayer = 0;
    pixelClusterIsSplit = 0;
    pixelClusterLocalX = 0;
    pixelClusterLocalY = 0;
    pixelClusterLocalErrorX = 0;
    pixelClusterLocalErrorY = 0;
    hitNContributingPtcs = 0;
    hitNContributingPU = 0;
    hitContributingPtcsBarcode = 0;
    pixelIBLIndex = 0;
    pixelIBLRow = 0;
    pixelIBLCol = 0;
    pixelIBLCharge = 0;
    pixelIBLToT = 0;
    pixelBLIndex = 0;
    pixelBLRow = 0;
    pixelBLCol = 0;
    pixelBLCharge = 0;
    pixelBLToT = 0;
    pixelL2Index = 0;
    pixelL2Row = 0;
    pixelL2Col = 0;
    pixelL2Charge = 0;
    pixelL2ToT = 0;
    pixelL3Index = 0;
    pixelL3Row = 0;
    pixelL3Col = 0;
    pixelL3Charge = 0;
    pixelL3ToT = 0;

    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
    fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
    fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
    fChain->SetBranchAddress("averagePU", &averagePU, &b_averagePU);
    fChain->SetBranchAddress("eventPU", &eventPU, &b_eventPU);
    fChain->SetBranchAddress("mcFlag", &mcFlag, &b_mcFlag);
    fChain->SetBranchAddress("pVtxX", &pVtxX, &b_pVtxX);
    fChain->SetBranchAddress("pVtxY", &pVtxY, &b_pVtxY);
    fChain->SetBranchAddress("pVtxZ", &pVtxZ, &b_pVtxZ);
    fChain->SetBranchAddress("pVtxXErr", &pVtxXErr, &b_pVtxXErr);
    fChain->SetBranchAddress("pVtxYErr", &pVtxYErr, &b_pVtxYErr);
    fChain->SetBranchAddress("pVtxZErr", &pVtxZErr, &b_pVtxZErr);
    fChain->SetBranchAddress("truthPVtxX", &truthPVtxX, &b_truthPVtxX);
    fChain->SetBranchAddress("truthPVtxY", &truthPVtxY, &b_truthPVtxY);
    fChain->SetBranchAddress("truthPVtxZ", &truthPVtxZ, &b_truthPVtxZ);
    fChain->SetBranchAddress("trackNPixelHits", &trackNPixelHits, &b_trackNPixelHits);
    fChain->SetBranchAddress("trackNPixelHoles", &trackNPixelHoles, &b_trackNPixelHoles);
    fChain->SetBranchAddress("trackNPixelLayers", &trackNPixelLayers, &b_trackNPixelLayers);
    fChain->SetBranchAddress("trackNPixelOutliers", &trackNPixelOutliers, &b_trackNPixelOutliers);
    fChain->SetBranchAddress("trackNIBLHits", &trackNIBLHits, &b_trackNIBLHits);
    fChain->SetBranchAddress("trackNSCTHits", &trackNSCTHits, &b_trackNSCTHits);
    fChain->SetBranchAddress("trackNSCTHoles", &trackNSCTHoles, &b_trackNSCTHoles);
    fChain->SetBranchAddress("trackNTRTHits", &trackNTRTHits, &b_trackNTRTHits);
    fChain->SetBranchAddress("trackNBLHits", &trackNBLHits, &b_trackNBLHits);
    fChain->SetBranchAddress("trackNSplitIBLHits", &trackNSplitIBLHits, &b_trackNSplitIBLHits);
    fChain->SetBranchAddress("trackNSplitBLHits", &trackNSplitBLHits, &b_trackNSplitBLHits);
    fChain->SetBranchAddress("trackNSharedIBLHits", &trackNSharedIBLHits, &b_trackNSharedIBLHits);
    fChain->SetBranchAddress("trackNSharedBLHits", &trackNSharedBLHits, &b_trackNSharedBLHits);
    fChain->SetBranchAddress("trackNL2Hits", &trackNL2Hits, &b_trackNL2Hits);
    fChain->SetBranchAddress("trackNL3Hits", &trackNL3Hits, &b_trackNL3Hits);
    fChain->SetBranchAddress("trackNECHits", &trackNECHits, &b_trackNECHits);
    fChain->SetBranchAddress("trackNIBLExpectedHits", &trackNIBLExpectedHits, &b_trackNIBLExpectedHits);
    fChain->SetBranchAddress("trackNBLExpectedHits", &trackNBLExpectedHits, &b_trackNBLExpectedHits);
    fChain->SetBranchAddress("trackNSCTSharedHits", &trackNSCTSharedHits, &b_trackNSCTSharedHits);
    fChain->SetBranchAddress("trackCharge", &trackCharge, &b_trackCharge);
    fChain->SetBranchAddress("trackPt", &trackPt, &b_trackPt);
    fChain->SetBranchAddress("trackPhi", &trackPhi, &b_trackPhi);
    fChain->SetBranchAddress("trackEta", &trackEta, &b_trackEta);
    fChain->SetBranchAddress("trackTheta", &trackTheta, &b_trackTheta);
    fChain->SetBranchAddress("trackqOverP", &trackqOverP, &b_trackqOverP);
    fChain->SetBranchAddress("trackD0", &trackD0, &b_trackD0);
    fChain->SetBranchAddress("trackZ0", &trackZ0, &b_trackZ0);
    fChain->SetBranchAddress("trackD0Err", &trackD0Err, &b_trackD0Err);
    fChain->SetBranchAddress("trackZ0Err", &trackZ0Err, &b_trackZ0Err);
    fChain->SetBranchAddress("trackqOverPErr", &trackqOverPErr, &b_trackqOverPErr);
    fChain->SetBranchAddress("trackDeltaZSinTheta", &trackDeltaZSinTheta, &b_trackDeltaZSinTheta);
    fChain->SetBranchAddress("trackPixeldEdx", &trackPixeldEdx, &b_trackPixeldEdx);
    fChain->SetBranchAddress("trackNPixeldEdx", &trackNPixeldEdx, &b_trackNPixeldEdx);
    fChain->SetBranchAddress("trueD0", &trueD0, &b_trueD0);
    fChain->SetBranchAddress("trueZ0", &trueZ0, &b_trueZ0);
    fChain->SetBranchAddress("truePhi", &truePhi, &b_truePhi);
    fChain->SetBranchAddress("trueTheta", &trueTheta, &b_trueTheta);
    fChain->SetBranchAddress("trueqOverP", &trueqOverP, &b_trueqOverP);
    fChain->SetBranchAddress("truePdgId", &truePdgId, &b_truePdgId);
    fChain->SetBranchAddress("trueBarcode", &trueBarcode, &b_trueBarcode);
    fChain->SetBranchAddress("truthMatchProb", &truthMatchProb, &b_truthMatchProb);
    fChain->SetBranchAddress("genVtxR", &genVtxR, &b_genVtxR);
    fChain->SetBranchAddress("genVtxZ", &genVtxZ, &b_genVtxZ);
    fChain->SetBranchAddress("parentFlavour", &parentFlavour, &b_parentFlavour);
    fChain->SetBranchAddress("minTrkdR", &minTrkdR, &b_minTrkdR);
    fChain->SetBranchAddress("hitSplitIBL", &hitSplitIBL, &b_hitSplitIBL);
    fChain->SetBranchAddress("hitSplitBL", &hitSplitBL, &b_hitSplitBL);
    fChain->SetBranchAddress("hitSplitL2", &hitSplitL2, &b_hitSplitL2);
    fChain->SetBranchAddress("hitSplitL3", &hitSplitL3, &b_hitSplitL3);
    fChain->SetBranchAddress("hitIsEndCap", &hitIsEndCap, &b_hitIsEndCap);
    fChain->SetBranchAddress("hitIsHole", &hitIsHole, &b_hitIsHole);
    fChain->SetBranchAddress("hitIsOutlier", &hitIsOutlier, &b_hitIsOutlier);
    fChain->SetBranchAddress("hitIs3D", &hitIs3D, &b_hitIs3D);
    fChain->SetBranchAddress("hitIsIBL", &hitIsIBL, &b_hitIsIBL);
    fChain->SetBranchAddress("hitLayer", &hitLayer, &b_hitLayer);
    fChain->SetBranchAddress("hitCharge", &hitCharge, &b_hitCharge);
    fChain->SetBranchAddress("hitToT", &hitToT, &b_hitToT);
    fChain->SetBranchAddress("hitLVL1A", &hitLVL1A, &b_hitLVL1A);
    fChain->SetBranchAddress("hitNPixelX", &hitNPixelX, &b_hitNPixelX);
    fChain->SetBranchAddress("hitNPixelY", &hitNPixelY, &b_hitNPixelY);
    fChain->SetBranchAddress("hitEtaModule", &hitEtaModule, &b_hitEtaModule);
    fChain->SetBranchAddress("hitPhiModule", &hitPhiModule, &b_hitPhiModule);
    fChain->SetBranchAddress("hitVBias", &hitVBias, &b_hitVBias);
    fChain->SetBranchAddress("hitVDep", &hitVDep, &b_hitVDep);
    fChain->SetBranchAddress("hitTemp", &hitTemp, &b_hitTemp);
    fChain->SetBranchAddress("hitLorentzShift", &hitLorentzShift, &b_hitLorentzShift);
    fChain->SetBranchAddress("hitIsSplit", &hitIsSplit, &b_hitIsSplit);
    fChain->SetBranchAddress("hitGlobalX", &hitGlobalX, &b_hitGlobalX);
    fChain->SetBranchAddress("hitGlobalY", &hitGlobalY, &b_hitGlobalY);
    fChain->SetBranchAddress("hitGlobalZ", &hitGlobalZ, &b_hitGlobalZ);
    fChain->SetBranchAddress("hitLocalX", &hitLocalX, &b_hitLocalX);
    fChain->SetBranchAddress("hitLocalY", &hitLocalY, &b_hitLocalY);
    fChain->SetBranchAddress("g4LocalX", &g4LocalX, &b_g4LocalX);
    fChain->SetBranchAddress("g4LocalY", &g4LocalY, &b_g4LocalY);
    fChain->SetBranchAddress("g4Barcode", &g4Barcode, &b_g4Barcode);
    fChain->SetBranchAddress("g4PdgId", &g4PdgId, &b_g4PdgId);
    fChain->SetBranchAddress("g4EnergyDeposit", &g4EnergyDeposit, &b_g4EnergyDeposit);
    fChain->SetBranchAddress("trkLocalX", &trkLocalX, &b_trkLocalX);
    fChain->SetBranchAddress("trkLocalY", &trkLocalY, &b_trkLocalY);
    fChain->SetBranchAddress("hitLocalErrorX", &hitLocalErrorX, &b_hitLocalErrorX);
    fChain->SetBranchAddress("hitLocalErrorY", &hitLocalErrorY, &b_hitLocalErrorY);
    fChain->SetBranchAddress("trkLocalErrorX", &trkLocalErrorX, &b_trkLocalErrorX);
    fChain->SetBranchAddress("trkLocalErrorY", &trkLocalErrorY, &b_trkLocalErrorY);
    fChain->SetBranchAddress("unbiasedResidualX", &unbiasedResidualX, &b_unbiasedResidualX);
    fChain->SetBranchAddress("unbiasedResidualY", &unbiasedResidualY, &b_unbiasedResidualY);
    fChain->SetBranchAddress("biasedResidualX", &biasedResidualX, &b_biasedResidualX);
    fChain->SetBranchAddress("biasedResidualY", &biasedResidualY, &b_biasedResidualY);
    fChain->SetBranchAddress("unbiasedPullX", &unbiasedPullX, &b_unbiasedPullX);
    fChain->SetBranchAddress("unbiasedPullY", &unbiasedPullY, &b_unbiasedPullY);
    fChain->SetBranchAddress("biasedPullX", &biasedPullX, &b_biasedPullX);
    fChain->SetBranchAddress("biasedPullY", &biasedPullY, &b_biasedPullY);
    fChain->SetBranchAddress("trk2G4DistX", &trk2G4DistX, &b_trk2G4DistX);
    fChain->SetBranchAddress("trk2G4DistY", &trk2G4DistY, &b_trk2G4DistY);
    fChain->SetBranchAddress("trk2DistX", &trk2DistX, &b_trk2DistX);
    fChain->SetBranchAddress("trk2DistY", &trk2DistY, &b_trk2DistY);
    fChain->SetBranchAddress("hit3DClusterDX", &hit3DClusterDX, &b_hit3DClusterDX);
    fChain->SetBranchAddress("hit3DClusterDY", &hit3DClusterDY, &b_hit3DClusterDY);
    fChain->SetBranchAddress("trkPhiOnSurface", &trkPhiOnSurface, &b_trkPhiOnSurface);
    fChain->SetBranchAddress("trkThetaOnSurface", &trkThetaOnSurface, &b_trkThetaOnSurface);
    fChain->SetBranchAddress("trkHitLocalX", &trkHitLocalX, &b_trkHitLocalX);
    fChain->SetBranchAddress("trkHitLocalY", &trkHitLocalY, &b_trkHitLocalY);
    fChain->SetBranchAddress("trkHitLocalErrorX", &trkHitLocalErrorX, &b_trkHitLocalErrorX);
    fChain->SetBranchAddress("trkHitLocalErrorY", &trkHitLocalErrorY, &b_trkHitLocalErrorY);
    fChain->SetBranchAddress("pixelClusterLayer", &pixelClusterLayer, &b_pixelClusterLayer);
    fChain->SetBranchAddress("pixelClusterIsSplit", &pixelClusterIsSplit, &b_pixelClusterIsSplit);
    fChain->SetBranchAddress("pixelClusterLocalX", &pixelClusterLocalX, &b_pixelClusterLocalX);
    fChain->SetBranchAddress("pixelClusterLocalY", &pixelClusterLocalY, &b_pixelClusterLocalY);
    fChain->SetBranchAddress("pixelClusterLocalErrorX", &pixelClusterLocalErrorX, &b_pixelClusterLocalErrorX);
    fChain->SetBranchAddress("pixelClusterLocalErrorY", &pixelClusterLocalErrorY, &b_pixelClusterLocalErrorY);
    fChain->SetBranchAddress("hitNContributingPtcs", &hitNContributingPtcs, &b_hitNContributingPtcs);
    fChain->SetBranchAddress("hitNContributingPU", &hitNContributingPU, &b_hitNContributingPU);
    fChain->SetBranchAddress("hitContributingPtcsBarcode", &hitContributingPtcsBarcode, &b_hitContributingPtcsBarcode);
    fChain->SetBranchAddress("pixelIBLIndex", &pixelIBLIndex, &b_pixelIBLIndex);
    fChain->SetBranchAddress("pixelIBLRow", &pixelIBLRow, &b_pixelIBLRow);
    fChain->SetBranchAddress("pixelIBLCol", &pixelIBLCol, &b_pixelIBLCol);
    fChain->SetBranchAddress("pixelIBLCharge", &pixelIBLCharge, &b_pixelIBLCharge);
    fChain->SetBranchAddress("pixelIBLToT", &pixelIBLToT, &b_pixelIBLToT);
    fChain->SetBranchAddress("pixelBLIndex", &pixelBLIndex, &b_pixelBLIndex);
    fChain->SetBranchAddress("pixelBLRow", &pixelBLRow, &b_pixelBLRow);
    fChain->SetBranchAddress("pixelBLCol", &pixelBLCol, &b_pixelBLCol);
    fChain->SetBranchAddress("pixelBLCharge", &pixelBLCharge, &b_pixelBLCharge);
    fChain->SetBranchAddress("pixelBLToT", &pixelBLToT, &b_pixelBLToT);
    fChain->SetBranchAddress("pixelL2Index", &pixelL2Index, &b_pixelL2Index);
    fChain->SetBranchAddress("pixelL2Row", &pixelL2Row, &b_pixelL2Row);
    fChain->SetBranchAddress("pixelL2Col", &pixelL2Col, &b_pixelL2Col);
    fChain->SetBranchAddress("pixelL2Charge", &pixelL2Charge, &b_pixelL2Charge);
    fChain->SetBranchAddress("pixelL2ToT", &pixelL2ToT, &b_pixelL2ToT);
    fChain->SetBranchAddress("pixelL3Index", &pixelL3Index, &b_pixelL3Index);
    fChain->SetBranchAddress("pixelL3Row", &pixelL3Row, &b_pixelL3Row);
    fChain->SetBranchAddress("pixelL3Col", &pixelL3Col, &b_pixelL3Col);
    fChain->SetBranchAddress("pixelL3Charge", &pixelL3Charge, &b_pixelL3Charge);
    fChain->SetBranchAddress("pixelL3ToT", &pixelL3ToT, &b_pixelL3ToT);
}