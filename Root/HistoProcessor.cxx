#include "ChargeCollection/HistoProcessor.h"
#include "ChargeCollection/Event.h"

#include <TFile.h>
#include <TChain.h>

#include <iomanip>
#include <iostream>
#include <fstream>

HistoProcessor::HistoProcessor(std::vector<std::string> InputFiles, std::string OutputFile, std::string RunNumber){
    m_InputFiles = InputFiles;
    m_OutputFile = OutputFile;
    m_RunNumber = RunNumber;
}

void HistoProcessor::RenameHistograms(){

    m_Corrected_ClusterCharge_SideAC_Disk1->SetName(("Corrected_ClusterCharge_SideAC_Disk1_RunNumber_"+ m_RunNumber).c_str());
    m_Corrected_ClusterCharge_SideAC_Disk2->SetName(("Corrected_ClusterCharge_SideAC_Disk2_RunNumber_"+ m_RunNumber).c_str());
    m_Corrected_ClusterCharge_SideAC_Disk3->SetName(("Corrected_ClusterCharge_SideAC_Disk3_RunNumber_"+ m_RunNumber).c_str());

    m_Corrected_ClusterCharge_SideA_Disk1->SetName(("Corrected_ClusterCharge_SideA_Disk1_RunNumber_"+ m_RunNumber).c_str());
    m_Corrected_ClusterCharge_SideA_Disk2->SetName(("Corrected_ClusterCharge_SideA_Disk2_RunNumber_"+ m_RunNumber).c_str());
    m_Corrected_ClusterCharge_SideA_Disk3->SetName(("Corrected_ClusterCharge_SideA_Disk3_RunNumber_"+ m_RunNumber).c_str());

    m_Corrected_ClusterCharge_SideC_Disk1->SetName(("Corrected_ClusterCharge_SideC_Disk1_RunNumber_"+ m_RunNumber).c_str());
    m_Corrected_ClusterCharge_SideC_Disk2->SetName(("Corrected_ClusterCharge_SideC_Disk2_RunNumber_"+ m_RunNumber).c_str());
    m_Corrected_ClusterCharge_SideC_Disk3->SetName(("Corrected_ClusterCharge_SideC_Disk3_RunNumber_"+ m_RunNumber).c_str());
}

void HistoProcessor::Fill_Histograms(Event event){

    for(uint ihitLayer=0; ihitLayer<event.hitLayer->size(); ihitLayer++){

        if(event.hitIsHole->at(ihitLayer)==0 and (event.hitIsEndCap->at(ihitLayer)==-2 or event.hitIsEndCap->at(ihitLayer)==+2)){
            
            auto SurfaceAngle = GetSurfaceAngle(event.trkPhiOnSurface->at(ihitLayer), event.trkThetaOnSurface->at(ihitLayer));
            SurfaceAngle = cos(SurfaceAngle)/1000;

            if(event.hitLayer->at(ihitLayer)==0){
                FillHistogram_UnderOverflowBin(m_Corrected_ClusterCharge_SideAC_Disk1, event.hitCharge->at(ihitLayer)*SurfaceAngle, 1, true, false);
            } 

            if(event.hitLayer->at(ihitLayer)==1){
                FillHistogram_UnderOverflowBin(m_Corrected_ClusterCharge_SideAC_Disk2, event.hitCharge->at(ihitLayer)*SurfaceAngle, 1, true, false);
            }

            if(event.hitLayer->at(ihitLayer)==2){
                FillHistogram_UnderOverflowBin(m_Corrected_ClusterCharge_SideAC_Disk3, event.hitCharge->at(ihitLayer)*SurfaceAngle, 1, true, false);
            }          
        }

        if(event.hitIsHole->at(ihitLayer)==0 and event.hitIsEndCap->at(ihitLayer)==-2){
            
            auto SurfaceAngle = GetSurfaceAngle(event.trkPhiOnSurface->at(ihitLayer), event.trkThetaOnSurface->at(ihitLayer));
            SurfaceAngle = cos(SurfaceAngle)/1000;

            if(event.hitLayer->at(ihitLayer)==0){
                FillHistogram_UnderOverflowBin(m_Corrected_ClusterCharge_SideA_Disk1, event.hitCharge->at(ihitLayer)*SurfaceAngle, 1, true, false);
            } 

            if(event.hitLayer->at(ihitLayer)==1){
                FillHistogram_UnderOverflowBin(m_Corrected_ClusterCharge_SideA_Disk2, event.hitCharge->at(ihitLayer)*SurfaceAngle, 1, true, false);
            }

            if(event.hitLayer->at(ihitLayer)==2){
                FillHistogram_UnderOverflowBin(m_Corrected_ClusterCharge_SideA_Disk3, event.hitCharge->at(ihitLayer)*SurfaceAngle, 1, true, false);
            }          
        }

        if(event.hitIsHole->at(ihitLayer)==0 and event.hitIsEndCap->at(ihitLayer)==2){
            
            auto SurfaceAngle = GetSurfaceAngle(event.trkPhiOnSurface->at(ihitLayer), event.trkThetaOnSurface->at(ihitLayer));
            SurfaceAngle = cos(SurfaceAngle)/1000;

            if(event.hitLayer->at(ihitLayer)==0){
                FillHistogram_UnderOverflowBin(m_Corrected_ClusterCharge_SideC_Disk1, event.hitCharge->at(ihitLayer)*SurfaceAngle, 1, true, false);
            } 

            if(event.hitLayer->at(ihitLayer)==1){
                FillHistogram_UnderOverflowBin(m_Corrected_ClusterCharge_SideC_Disk2, event.hitCharge->at(ihitLayer)*SurfaceAngle, 1, true, false);
            }

            if(event.hitLayer->at(ihitLayer)==2){
                FillHistogram_UnderOverflowBin(m_Corrected_ClusterCharge_SideC_Disk3, event.hitCharge->at(ihitLayer)*SurfaceAngle, 1, true, false);
            }          
        }
    }
}

void HistoProcessor::Write_Histograms(){

    m_Corrected_ClusterCharge_SideAC_Disk1->Write();
    m_Corrected_ClusterCharge_SideAC_Disk2->Write();
    m_Corrected_ClusterCharge_SideAC_Disk3->Write();

    m_Corrected_ClusterCharge_SideA_Disk1->Write();
    m_Corrected_ClusterCharge_SideA_Disk2->Write();
    m_Corrected_ClusterCharge_SideA_Disk3->Write();

    m_Corrected_ClusterCharge_SideC_Disk1->Write();
    m_Corrected_ClusterCharge_SideC_Disk2->Write();
    m_Corrected_ClusterCharge_SideC_Disk3->Write();
}

void HistoProcessor::FillHistogram_UnderOverflowBin(TH1D* Histogram, float Variable, float Weight, bool Overflow_Flag, bool Underflow_Flag){

    if(Overflow_Flag == true and Underflow_Flag == false){

        auto Max = Histogram->GetXaxis()->GetXmax();
        auto Min = Histogram->GetXaxis()->GetXmin();
        auto Bins = Histogram->GetNbinsX();
        auto MaxValue = Min + (Max-Min)/(float)Bins*(float)(Bins-1);

        if(Variable < Max) Histogram->Fill(Variable, Weight);
        if(Variable >= Max) Histogram->Fill(MaxValue, Weight);
    }

    if(Overflow_Flag == false and Underflow_Flag == true){

        auto Max = Histogram->GetXaxis()->GetXmax();
        auto Min = Histogram->GetXaxis()->GetXmin();
        auto Bins = Histogram->GetNbinsX();
        auto MinValue = Min + (Max-Min)/(float)Bins;

        if(Variable > Min) Histogram->Fill(Variable, Weight);
        if(Variable <= Min) Histogram->Fill(MinValue, Weight);
    }

    if(Overflow_Flag == true and Underflow_Flag == true){

        auto Max = Histogram->GetXaxis()->GetXmax();
        auto Min = Histogram->GetXaxis()->GetXmin();
        auto Bins = Histogram->GetNbinsX();
        auto MaxValue = Min + (Max-Min)/(float)Bins*(float)(Bins-1);
        auto MinValue = Min + (Max-Min)/(float)Bins;

        if(Variable < Max) Histogram->Fill(Variable, Weight);
        if(Variable >= Max) Histogram->Fill(MaxValue, Weight);
        if(Variable > Min) Histogram->Fill(Variable, Weight);
        if(Variable <= Min) Histogram->Fill(MinValue, Weight);
    }

    if(Overflow_Flag == false and Underflow_Flag == false){
        Histogram->Fill(Variable, Weight);
    }
}

double HistoProcessor::GetSurfaceAngle(double Phi, double Theta){

        double SurfaceAngle;
        SurfaceAngle=atan(sqrt(tan(Phi)*tan(Phi)+tan(Theta)*tan(Theta)));

        return SurfaceAngle;
}

void HistoProcessor::Process(){

    std::unique_ptr<TFile> Outfile(TFile::Open(m_OutputFile.c_str(), "RECREATE"));
    TChain *chain = new TChain("trktree");

    for(auto const &fname : m_InputFiles){
        std::cout << "INFO\t" <<  "File " << fname << " is added to the current chain." <<std::endl; 
        chain->Add(fname.c_str());
    }

    Event event(chain);
    unsigned int Entries = chain->GetEntries();

    RenameHistograms();

    for(unsigned int iEvent = 0; iEvent < Entries; iEvent++){

        if(iEvent%200000 == 0) {
	        std::cout << "INFO\t" <<  iEvent << " / " << Entries << " events processed ( " << lround(100*iEvent/Entries) << " % ) " << std::endl;
        }
        event.GetEntry(iEvent);
        if(event.runNumber != stoi(m_RunNumber)) continue;
        Fill_Histograms(event);
    }

    Write_Histograms();

    Outfile->Write();
    Outfile->Close();
}