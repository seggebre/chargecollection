#include <fstream>
#include <iostream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <dirent.h>

#include "ChargeCollection/InputOptions.h"

namespace ttHML_InputOptions{

    void PrintUsage(){
        std::cout << "You need to three arguments in the following order:" <<std::endl;
        std::cout << "1. A .txt File containg a List of Input Samples. You can pass multiple in one line, but separate them by a comma." <<std::endl;
        std::cout << "2. A .txt File containg a List of RunNumbers." <<std::endl;
        std::cout << "3. The Name of the Output Directory, where the Histograms are stored." <<std::endl;
    }

    std::vector<std::string> GetFileContent(std::string FileName){

        std::vector<std::string> Content;
        std::string item_name;
        std::ifstream file_name;
        file_name.open(FileName.c_str());
        std::string line;

        while(getline(file_name, line)){
            Content.push_back(line);
        }

        return Content;
    }

    std::vector<std::string> SplitInput(std::string& s, char delimiter){                                                                                                                                                                                             
        std::vector<std::string> splits;                                                                                                                                                           
        std::string split;                                                                                                                                                                         
        std::istringstream ss(s);                                                                                                                                                                  
        
        while (std::getline(ss, split, delimiter)){                                                                                                                                                                                          
            splits.push_back(split);                                                                                                                                                                
        }                                                                                                                                                                                          
        return splits;                                                                                                                                                                             
    }

    ConfigOptions get_config_opts(int argc, char* argv[]){

        if(argc != 4){
            PrintUsage();
            exit(EXIT_FAILURE);
        }

        auto InputFiles = GetFileContent(argv[1]);
        auto InputRunNumbers = GetFileContent(argv[2]);
        std::string OutputDirectory = argv[3];

        if(InputFiles.size() != InputRunNumbers.size()){
            std::cout << "Number of Input Files and RunNumbers does not match." << std::endl;
            exit(EXIT_FAILURE);
        }

        ConfigOptions opts;
        opts.InputFiles = InputFiles;
        opts.InputRunNumbers = InputRunNumbers;
        opts.OutputDirectory = OutputDirectory;

        return opts;
    }

}