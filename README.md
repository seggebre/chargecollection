## Compiling the code
To compile the code you need to setup `ROOT` and `cmake`.
On lxplus type:

```
lsetup cmake
lsetup "views LCG_102 x86_64-centos7-gcc11-opt"
```

or use the setup script.
The code can be compiled by:

```
mkdir build
cd build 
cmake ../
make -j4
```

## Generating Histograms
To generate histograms, you need to pass three arguments in the following order:

1. A .txt file containg a list of input samples. You can pass multiple in one line, but separate them by a comma.
2. A .txt file containg a list of run-numbers.
3. The name of the output directory, where the histograms are stored.

Example files can be found in the `scripts` directory.
The histograms for the three disks for each side of the detector and a combination of both sides, is stored in the passed directory.
You can execute the code with the following command:

```
./GenerateDistributions <Your_InputList.txt> <Your_RunNumberList.txt> <Fancy_OutputDirectory>
```

The histograms are filled with the corrected cluster charge in units of ke.